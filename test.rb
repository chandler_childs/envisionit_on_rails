class Animal
    
    @sound = "???"
    
    def initialize(type, name)
        @type = type
        @name = name
        
    end
    
    def breathing
        puts "I'm breathing"
    end
    
    def to_string
        puts "This #{@type} is named #{@name}."
    end
    
    def speak
       puts "The #{@type} says #{@sound}." 
    end
end

class Dog < Animal
    def initialize(name)
        @name = name
        @type = "dog"
        @sound = "woof"
    end
end

class Cat < Animal
    def initialize(name)
        @name = name
        @type = "cat"
        @sound = "meow"
    end
end

a = Animal.new('dog', 'Barkly')
b = Dog.new("Lexus")

c = Cat.new('aragorn')

zoo = []

zoo << a
zoo << b
zoo << c


zoo.each do |x|
    x.to_string
   x.speak 
end